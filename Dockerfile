FROM quay.io/ansible/awx-ee:latest

USER root

# install OS binaries
RUN yum -y install \
ca-certificates \
gcc \
git \
git-lfs \
krb5-devel \
krb5-libs \
krb5-workstation \
libcurl-devel \
libxml2-devel \
openssl-devel \
python3-jmespath \
python3-netaddr \
# python3-passlib \
python3-pycurl \
# python38-devel \
# python38-pytz \
# python38-pyyaml \
# python38-requests \
qemu-img

# add Python dependencies and Ansible
# Galary dependencies
ADD requirements.yml /tmp/requirements.yml
ADD requirements.txt /tmp/requirements.txt

# upgrade pip
RUN /usr/bin/python3 -m pip install --upgrade pip

# install Ansible Galaxy collections
RUN ansible-galaxy collection install -r /tmp/requirements.yml --collections-path /usr/share/ansible/collections

# install Python dependencies
RUN pip install -r /tmp/requirements.txt

# add certificates
RUN update-ca-trust force-enable
RUN chmod -R 0777 /usr/share/ansible/collections

RUN dnf -y check-update; exit 0
RUN dnf -y update

USER 1000