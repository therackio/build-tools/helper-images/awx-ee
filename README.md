# awx-ee
https://www.linkedin.com/pulse/creating-custom-ee-awx-phil-griffiths/

On Windows, need to run from a WSL2 env (Ubuntu 20.04) with Docker WSL integration - 
https://stackoverflow.com/questions/63497928/ubuntu-wsl-with-docker-could-not-be-found

Also ensure all files are LF, not CRLF

edit /home/bob/.local/lib/python3.8/site-packages/ansible_builder if needed


```
source builder/Scripts/activate
python3 -m pip install -r requirements.txt
ansible-builder build --tag registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 --context ./context --container-runtime docker --build-arg ANSIBLE_RUNNER_IMAGE=quay.io/ansible/awx-ee:0.2.0
docker build -f ./context/Dockerfile -t registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 ./context
docker run --rm -v /home/bob/.local/lib/python3.8/site-packages/ansible_builder:/ansible_builder_mount:Z registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 python3 /ansible_builder_mount/introspect.py
docker build -f ./context/Dockerfile -t registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 ./context
docker push registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0
```

```
DOCKER_CONTENT_TRUST=1 DOCKER_BUILDKIT=1 docker image build --no-cache -t registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:latest-custom .
docker push -a registry.gitlab.com/therackio/build-tools/helper-images/awx-ee
```












