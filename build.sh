#!/bin/bash
ansible-builder build --tag registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 --context ./context --container-runtime docker --build-arg ANSIBLE_RUNNER_IMAGE=quay.io/ansible/awx-ee:0.2.0
docker build -f ./context/Dockerfile -t registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 ./context
docker run --rm -v /home/bob/.local/lib/python3.8/site-packages/ansible_builder:/ansible_builder_mount:Z registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 python3 /ansible_builder_mount/introspect.py
docker build -f ./context/Dockerfile -t registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0 ./context
docker push registry.gitlab.com/therackio/build-tools/helper-images/awx-ee:0.2.0